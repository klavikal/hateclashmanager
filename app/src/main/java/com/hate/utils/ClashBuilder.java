package com.hate.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Chris on 2/28/2019.
 */

public class ClashBuilder {

    private final ArrayList<String> _players;
    private final ArrayList<Clash> _clashes;

    public ClashBuilder(){
        _players = new ArrayList<String>();
        _clashes = new ArrayList<Clash>();
    }

    public void addPlayer(String player){

        if(_players.contains(player) || (player == null || player.isEmpty()) )
            return;

        _players.add(player);
    }

    public void clear(){
        _players.clear();
        _clashes.clear();
    }

    public int getPlayerCount(){
        return _players.size();
    }

    public int getClashCount(){
        return _clashes.size();
    }

    public List<Clash> getClashes(){
        return Collections.unmodifiableList(_clashes);
    }

    public List<String> getPlayers(){
        return Collections.unmodifiableList(_players);
    }

    public void buildClashes(){

        _clashes.clear();

        int clashesNeeded = getNumberOfClashesNeeded();
        if(clashesNeeded < 0 )
            return;

        for (int i=0;i<clashesNeeded; i++)
            BuildPairs();
    }

    // Per the HATE rulebook how many clashes between pairs of players
    private int getNumberOfClashesNeeded(){

        switch (_players.size()){
            case 2: return 5;
            case 3: return 2;
            case 4:
            case 5:
            case 6: return 1;
        }

        return -1;
    }

    private void BuildPairs() {
        int endOfList = _players.size() - 1;

        for (int j = 0; j< endOfList; j++) {
            for (int i = j+1; i<= endOfList; i++){
                String player1 = _players.get(j);
                String player2 = _players.get(i);
                _clashes.add(new Clash(player1, player2, false));
            }
        }
    }

    public void loadClashes(ArrayList<Clash> loadedClashes) {

        for(Clash clash : loadedClashes){

            addPlayer(clash.getPlayer1());
            addPlayer(clash.getPlayer2());

            _clashes.add(clash);
        }
    }
}
