package com.hate.utils;

import java.io.Serializable;

/**
 * Created by Chris on 2/28/2019.
 */


public class Clash implements Serializable {

    // SerialVersionUID
    private static final long serialVersionUID = 19L;

    private final String player1;
    private final String player2;
    private boolean _isCompleted;

    public Clash(String player1, String player2, boolean isCompleted){

        this.player1 = player1;
        this.player2 = player2;
        _isCompleted = isCompleted;
    }

    public boolean isCompleted () {return _isCompleted;}

    public void setIsCompleteClash(boolean isComplete) {_isCompleted = isComplete;}

    public String getPlayer1() {
        return player1;
    }

    public String getPlayer2() {
        return player2;
    }

    @Override
    public String toString() {
        return player1.trim() + " -> " + player2.trim();
    }
}
