package com.hate.clashmanager;

import com.hate.utils.Clash;
import com.hate.utils.ClashBuilder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Created by Chris on 3/2/2019.
 */

public class SaveAppDataHelper {


    private String _filePath;

    public SaveAppDataHelper(String filePath){

        _filePath = filePath;
    }

    public void saveClashData(ClashBuilder builder){
        File file = new File(_filePath);
        FileOutputStream fileOutStream = null;

        try {
            fileOutStream = new FileOutputStream(file);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutStream);
            objectOutputStream.writeObject(new ArrayList<Clash>(builder.getClashes()));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Clash> loadClashData(){

        File file = new File(_filePath);
        FileInputStream fileInStream = null;

        if(!file.exists())
            return null;

        try {

            fileInStream = new FileInputStream(file);

            ObjectInputStream ois = new ObjectInputStream(fileInStream);
            ArrayList<Clash> restored = (ArrayList<Clash>) ois.readObject();
            return restored;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }
}
