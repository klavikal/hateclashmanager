package com.hate.clashmanager;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

public class AddPlayerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_player);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button button = (Button) findViewById(R.id.btnConfirm);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Intent resultIntent = new Intent();
               resultIntent.putExtra("Players", getPlayers());
               setResult(Activity.RESULT_OK, resultIntent);
               finish();
            }
        });

        loadIntentExtras();

    }

    private void loadIntentExtras() {
        Intent intent = getIntent();
        String[] players = intent.getStringArrayExtra("PlayersToLoad");

        if(players == null || players.length <= 0)
            return;

        int[] editTextIds = getEditTextIds();
        for (int i = 0; i < players.length; i++) {
            getEditText(editTextIds[i]).setText(players[i]);
        }

    }

    private ArrayList<String> getPlayers(){

        int[] editTextIds = getEditTextIds();

        ArrayList<String> players = new ArrayList<String>();

        for (int id : editTextIds) {
            String player = getEditText(id).getText().toString();
            players.add(player);
        }

        return players;
    }

    private EditText getEditText(int id){
        return (EditText) findViewById(id);
    }

    private int[] getEditTextIds() {
        return new int[]{R.id.et_player1,R.id.et_player2,R.id.et_player3,
                    R.id.et_player4,R.id.et_player5,R.id.et_player6};
    }

}
