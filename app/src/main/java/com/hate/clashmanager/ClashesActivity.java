package com.hate.clashmanager;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.hate.utils.Clash;
import com.hate.utils.ClashBuilder;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

public class ClashesActivity extends AppCompatActivity {

    private ClashBuilder _clashBuilder;
    private SaveAppDataHelper _saveAppData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clashes);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        _clashBuilder = new ClashBuilder();
        _saveAppData = new SaveAppDataHelper(new File(getFilesDir(),"clashData").getPath());

        importData();
    }

    @Override
    protected void onStop() {
        super.onStop();
        _saveAppData.saveClashData(_clashBuilder);
    }

    private void importData(){
        FileInputStream fis = null;
            ArrayList<Clash> clashes = _saveAppData.loadClashData();

            if(clashes == null || clashes.size() == 0)
                return;

            _clashBuilder.loadClashes(clashes);

            populateListWithClashes();
    }

    private void populateListWithClashes() {

        List<Clash> clashes = _clashBuilder.getClashes();

        ArrayAdapter<Clash> clashesAdapter = new ArrayAdapter<Clash>(ClashesActivity.this, android.R.layout.simple_list_item_multiple_choice, clashes);
        ListView lv = (ListView) findViewById(R.id.lvClashes);

        lv.setAdapter(clashesAdapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Clash checkedClash = (Clash) adapterView.getItemAtPosition(i);
                checkedClash.setIsCompleteClash(!checkedClash.isCompleted());
            }
        });

        for (int i=0; i<lv.getCount(); i++){
            Clash clash = (Clash) lv.getItemAtPosition(i);
            lv.setItemChecked(i,clash.isCompleted());
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_clashes, menu);
        return true;
    }

    private void startAddPlayerActivity() {
        Intent intent = new Intent(this, AddPlayerActivity.class);
        String[] players = _clashBuilder.getPlayers().toArray(new String[_clashBuilder.getPlayerCount()]);
        intent.putExtra("PlayersToLoad", players);
        startActivityForResult(intent, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode != 0 || resultCode != Activity.RESULT_OK)
            return;

        _clashBuilder.clear();

        ArrayList<String> players = data.getStringArrayListExtra("Players");

        for (String player : players) {
            _clashBuilder.addPlayer(player);
        }

        _clashBuilder.buildClashes();

        populateListWithClashes();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add_players) {
            startAddPlayerActivity();
        }

        return super.onOptionsItemSelected(item);
    }
}
