package com.hate.clashmanager;

import com.hate.utils.Clash;
import com.hate.utils.ClashBuilder;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    @Test
    public void twoPlayerClash_isBuiltCorrectly() {

        ClashBuilder builder = new ClashBuilder();

        assertNotNull(builder);

        builder.addPlayer("A");
        builder.addPlayer("B");

        int playerCt = builder.getPlayerCount();
        assertEquals(playerCt, 2);

        builder.buildClashes();

        int clashCount = builder.getClashCount();
        assertEquals(5, clashCount);
    }

    @Test
    public void threePlayerClash_isBuiltCorrectly() {
        ClashBuilder builder = new ClashBuilder();

        assertNotNull(builder);

        builder.addPlayer("A");
        builder.addPlayer("B");
        builder.addPlayer("C");

        int playerCt = builder.getPlayerCount();
        assertEquals(3, playerCt);

        builder.buildClashes();

        int clashCount = builder.getClashCount();
        assertEquals(6, clashCount);

        List<Clash> clashes = builder.getClashes();

        ArrayList<Clash> p1Clashes = new ArrayList<Clash>();
        ArrayList<Clash> p2Clashes = new ArrayList<Clash>();
        ArrayList<Clash> p3Clashes = new ArrayList<Clash>();

        for (Clash clash : clashes) {
            if (clash.getPlayer1().equals("A") && clash.getPlayer2().equals("B"))
                p1Clashes.add(clash);
            else if (clash.getPlayer1().equals("A") && clash.getPlayer2().equals("C"))
                p2Clashes.add(clash);
            else if (clash.getPlayer1().equals("B") && clash.getPlayer2().equals("C"))
                p3Clashes.add(clash);
        }

        assertEquals(2, p1Clashes.size());
        assertEquals(2, p2Clashes.size());
        assertEquals(2, p3Clashes.size());
    }

    @Test
    public void fivePlayerClash_isBuiltCorrectly() {
        ClashBuilder builder = new ClashBuilder();

        assertNotNull(builder);

        builder.addPlayer("A");
        builder.addPlayer("B");
        builder.addPlayer("C");
        builder.addPlayer("D");
        builder.addPlayer("E");

        int playerCt = builder.getPlayerCount();
        assertEquals(5, playerCt);

        builder.buildClashes();

        int clashCount = builder.getClashCount();
        assertEquals(10, clashCount);

        List<Clash> clashes = builder.getClashes();

        final String[] playerPairs = new String[]{"A,B", "A,C", "A,D", "A,E", "B,C", "B,D", "B,E", "C,D", "C,E", "D,E"};

        assertEquals(10, playerPairs.length);

        for (String playerPair : playerPairs) {
            int count = 0;

            String p1 = playerPair.split(",")[0];
            String p2 = playerPair.split(",")[1];

            for (Clash clash : clashes) {
                if (clash.getPlayer1().equals(p1) && clash.getPlayer2().equals(p2))
                    count++;
            }

            assertEquals(1, count);
        }
    }

    @Test
    public void sixPlayerClash_isBuiltCorrectly() {
        ClashBuilder builder = new ClashBuilder();

        assertNotNull(builder);

        builder.addPlayer("A");
        builder.addPlayer("B");
        builder.addPlayer("C");
        builder.addPlayer("D");
        builder.addPlayer("E");
        builder.addPlayer("F");

        int playerCt = builder.getPlayerCount();
        assertEquals(6, playerCt);

        builder.buildClashes();

        int clashCount = builder.getClashCount();
        assertEquals(15, clashCount);

        List<Clash> clashes = builder.getClashes();

        final String[] playerPairs = new String[]{"A,B", "A,C", "A,D", "A,E", "A,F", "B,C", "B,D", "B,E", "B,F", "C,D", "C,E", "C,F", "D,E", "D,F", "E,F"};

        assertEquals(15, playerPairs.length);

        for (String playerPair : playerPairs) {
            int count = 0;

            String p1 = playerPair.split(",")[0];
            String p2 = playerPair.split(",")[1];

            for (Clash clash : clashes) {
                if (clash.getPlayer1().equals(p1) && clash.getPlayer2().equals(p2))
                    count++;
            }

            assertEquals(1, count);
        }
    }

    @Test
    public void testClash_serializeable(){
        Clash clash = new Clash("A", "B", false);

        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new ByteArrayOutputStream());
            assertNotNull(objectOutputStream);
            objectOutputStream.writeObject(clash);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testClashList_serializeable(){
        Clash clash = new Clash("A", "B", false);
        Clash clash2 = new Clash("A", "C", false);
        Clash clash3 = new Clash("B", "C", false);

        ArrayList<Clash> list = new ArrayList<>();
        list.add(clash);
        list.add(clash2);
        list.add(clash3);

        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new ByteArrayOutputStream());
            assertNotNull(objectOutputStream);
            objectOutputStream.writeObject(list);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testClashList_deSerializeable(){
        Clash clash = new Clash("A", "B", false);
        Clash clash2 = new Clash("A", "C", false);
        Clash clash3 = new Clash("B", "C", false);

        ArrayList<Clash> list = new ArrayList<>();
        list.add(clash);
        list.add(clash2);
        list.add(clash3);

        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(out);
            assertNotNull(objectOutputStream);
            objectOutputStream.writeObject(list);

            ByteArrayInputStream bais = new ByteArrayInputStream(out.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bais);
            ArrayList<Clash> restored = (ArrayList<Clash>) ois.readObject();

            assertNotNull(restored);
            assertEquals(restored.size(),3);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void clashBuilder_loadClashes(){

        Clash clash = new Clash("A", "B", false);
        Clash clash2 = new Clash("A", "C", false);
        Clash clash3 = new Clash("B", "C", false);

        ArrayList<Clash> list = new ArrayList<>();
        list.add(clash);
        list.add(clash2);
        list.add(clash3);

        ClashBuilder clashBuilder = new ClashBuilder();
        clashBuilder.loadClashes(list);

        assertNotNull(clashBuilder.getClashes());
        assertEquals(clashBuilder.getClashCount(),3);

    }


}